# Cloudflare Worker

## What is this?

This is a repository, where resides a simple script; which is used to configure Cloudflare Worker.
This Worker receives form input from [BLZR.SBS/CONTACT](https://blzr.sbs/contact/), and produce an update to Telegram via a Bot.

## Steps to reproduce if you want similar experience:

1. Clone this Repo (of course)
2. Make a `wrangler.toml` file and fill with below contents
```toml
name = "{NAME OF THE WORKER}"
main = "index.js"
compatibility_date = "2022-07-12"

[vars]
CHATID = "{CHAT_ID}"
ORIGIN_URI = "https://{YOUR_DOMAIN.COM}"
TELEGRAMTOKEN = "{BOT TOKEN FROM BOTFATHER}"
```
3. Go to `https://gitlab.com/{USERNAME}/cloudflare-worker/-/settings/ci_cd` and Click Expand on `Variables`
4. Add Below environment variables

    * `CF_ZONE_ID`
    * `CLOUDFLARE_ACCOUNT_ID`
    * `CLOUDFLARE_API_TOKEN`

To find ZONE ID and ACCOUNT ID, follow [this guide](https://developers.cloudflare.com/fundamentals/setup/find-account-and-zone-ids/). To get API TOKEN, follow [this guide](https://developers.cloudflare.com/workers/wrangler/migration/v1-to-v2/wrangler-legacy/authentication/#generate-tokens).

5. On the same CI/CD Settings page, Expand `Secure Files`. Upload `wrangler.toml` there.
6. Finally push the changes to Gitlab repository. Make one if not already created and then push the script.